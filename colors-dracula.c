/* normal foreground - background */
static const char col_nb[]         = "#282a36";
static const char col_nf[]         = "#f8f8f2";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#4d4d4d";

/* active foreground - background */
static const char col_ab[]         = "#4d4d4d";
static const char col_af[]         = "#5af78e";
