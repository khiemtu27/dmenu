/* normal foreground - background */
static const char col_nb[]         = "#2e3440";
static const char col_nf[]         = "#d8dee9";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#4c566a";

/* active foreground - background */
static const char col_ab[]         = "#3b4252";
static const char col_af[]         = "#a3be8c";
