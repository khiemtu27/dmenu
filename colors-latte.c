/* normal foreground - background */
static const char col_nb[]         = "#eff1f5";
static const char col_nf[]         = "#6c6f85";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#8c8fa1";

/* active foreground - background */
static const char col_ab[]         = "#40a02b";
static const char col_af[]         = "#dce0e8";
