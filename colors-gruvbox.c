/* normal foreground - background */
static const char col_nb[]         = "#1d2021";
static const char col_nf[]         = "#d5c4a1";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#504945";

/* active foreground - background */
static const char col_ab[]         = "#282828";
static const char col_af[]         = "#b8bb26";
