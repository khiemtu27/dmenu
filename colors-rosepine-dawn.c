/* normal foreground - background */
static const char col_nb[]         = "#faf4ed";
static const char col_nf[]         = "#cecacd";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#cecacd";

/* active foreground - background */
static const char col_ab[]         = "#d7827e";
static const char col_af[]         = "#fffaf3";
