/* normal foreground - background */
static const char col_nb[]         = "#1e1e2e";
static const char col_nf[]         = "#cdd6f4";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#585b70";

/* active foreground - background */
static const char col_ab[]         = "#45475a";
static const char col_af[]         = "#a6e3a1";
