/* normal foreground - background */
static const char col_nb[]         = "#1f2416";
static const char col_nf[]         = "#9d9d7a";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#5d6652";

/* active foreground - background */
static const char col_ab[]         = "#313729";
static const char col_af[]         = "#c5cd79";
