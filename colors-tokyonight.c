/* normal foreground - background */
static const char col_nb[]         = "#1a1b26";
static const char col_nf[]         = "#a9b1d6";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#414868";

/* active foreground - background */
static const char col_ab[]         = "#15161e";
static const char col_af[]         = "#bae68b";
