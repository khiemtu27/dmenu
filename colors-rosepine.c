/* normal foreground - background */
static const char col_nb[]         = "#191724";
static const char col_nf[]         = "#908caa";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#524f67";

/* active foreground - background */
static const char col_ab[]         = "#403d52";
static const char col_af[]         = "#9ccfd8";
