/* normal foreground - background */
static const char col_nb[]         = "#282c34";
static const char col_nf[]         = "#abb2bf";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#3e4452";

/* active foreground - background */
static const char col_ab[]         = "#2c323c";
static const char col_af[]         = "#98c379";
