/* normal foreground - background */
static const char col_nb[]         = "#eef3f6";
static const char col_nf[]         = "#4f555a";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#b2b7bb";

/* active foreground - background */
static const char col_ab[]         = "#e2e9ec";
static const char col_af[]         = "#6892c0";
