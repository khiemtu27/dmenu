/* normal foreground - background */
static const char col_nb[]         = "#232a2e";
static const char col_nf[]         = "#d3c6aa";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#7a8478";

/* active foreground - background */
static const char col_ab[]         = "#272e33";
static const char col_af[]         = "#a7c080";
