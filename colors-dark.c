/* normal foreground - background */
static const char col_nb[]         = "#15181a";
static const char col_nf[]         = "#abb2bf";

/* dimmed foreground - layout symbol */
static const char col_df[]         = "#484f5b";

/* active foreground - background */
static const char col_ab[]         = "#252a34";
static const char col_af[]         = "#79c38f";
